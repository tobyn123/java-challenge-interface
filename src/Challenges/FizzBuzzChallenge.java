package Challenges;
//Implements Challenge Interface
public class FizzBuzzChallenge implements Challenge {

    @Override
    public void challenge() {
        Integer numbers_to_fizz = 100;
        Integer fizz = 3, buzz = 5;
        for (int i = 1; i < numbers_to_fizz+1; i++) {
            if(i % fizz == 0 && i % buzz == 0)
                System.out.println("FIZZBUZZ");
            else if(i % fizz == 0)
                System.out.println("fizz");
            else if(i % buzz == 0)
                System.out.println("buzz");
            else
                System.out.println(i);
        }
    }

    @Override
    public void challengeIntro() { System.out.println("FizzBuzz challenge is about to run.."); }

    @Override
    public void challengeOut() { System.out.println("FizzBuzz is now complete!"); }

}
