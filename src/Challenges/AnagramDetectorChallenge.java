package Challenges;

import java.util.Arrays;

public class AnagramDetectorChallenge implements Challenge {

    @Override
    public void challenge() {
        String x = "23434";
        String y = "23434";
        String word1[] = x.split("");
        String word2[] = y.split("");
        Arrays.sort(word1);
        Arrays.sort(word2);

        if(Arrays.equals(word2, word1)) System.out.println("Anagram Detected!");
        else System.out.println("That's not an anagram!");
    }

    @Override
    public void challengeIntro() {
        System.out.println("Welcome to the Anagram Detector...");
    }

    @Override
    public void challengeOut() {
        System.out.println("Anagram Detector finished.");
    }


}
