package Challenges;
//Implements Challenge Interface
public class ReverseStringChallenge implements Challenge {

    @Override
    public void challenge() {
        String input = "Me reverse nicely please";
        String[] str_arr = input.split(" ");    //Split the string by whitespace.
        Integer str_len = str_arr.length;   //Number of words.
        String output = ""; //Initialise output.

        for (int i = 0; i < str_len; i++) {
            output = output.concat( (str_arr[(str_len - 1) - i]) + " "); //add word in array[i] to output string.
        }
        System.out.println(output);
    }

    @Override
    public void challengeIntro(){ System.out.println("Reverse String Challenge running..."); }

    @Override
    public void challengeOut() { System.out.println("Reverse String Challenge complete!"); }

}
