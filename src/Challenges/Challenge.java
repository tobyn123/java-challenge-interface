package Challenges;

public interface Challenge {

    public void challengeIntro();

    public void challenge();

    public void challengeOut();

}
