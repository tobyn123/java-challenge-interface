package challenger;

import Challenges.AnagramDetectorChallenge;
import Challenges.Challenge;

public class ChallengeRunner {

    public void RunChallenge(Challenge chal){
        chal.challengeIntro();
        chal.challenge();
        chal.challengeOut();

    }

}
