import Challenges.AnagramDetectorChallenge;
import Challenges.FizzBuzzChallenge;
import Challenges.ReverseStringChallenge;
import challenger.ChallengeRunner;

public class Main {

    public static void main(String[] args) {

        //Challenge Runner
        ChallengeRunner cRun = new ChallengeRunner();

        //Challenge 1 - The Reverse String Challenge
        ReverseStringChallenge firstChallenge = new ReverseStringChallenge();
        cRun.RunChallenge(firstChallenge);

        //Challenge 2 - FizzBuzz Challenge
        FizzBuzzChallenge secondChallenge = new FizzBuzzChallenge();
        cRun.RunChallenge(secondChallenge);

        //Challenge 3 - thirdChallenge
        AnagramDetectorChallenge thirdChallenge = new AnagramDetectorChallenge();
        cRun.RunChallenge(thirdChallenge);





    }
}
